package ru.bokhan.tm;

import ru.bokhan.tm.constant.TerminalConst;
import ru.bokhan.tm.model.TerminalCommand;
import ru.bokhan.tm.util.NumberUtil;

import java.util.Scanner;

public class Application {

    public static void main(final String[] args) {
        System.out.println("** WELCOME TO TASK MANAGER **");
        if (parseArgs(args)) exit();
        while (true) {
            final Scanner scanner = new Scanner(System.in);
            final String command = scanner.nextLine();
            parseCommand(command);
        }
    }

    public static void parseArg(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case TerminalConst.ARG_HELP:
                showHelp();
                break;
            case TerminalConst.ARG_ABOUT:
                showAbout();
                break;
            case TerminalConst.ARG_VERSION:
                showVersion();
                break;
            case TerminalConst.ARG_INFO:
                showInfo();
                break;
        }
    }

    public static void parseCommand(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case TerminalConst.CMD_HELP:
                showHelp();
                break;
            case TerminalConst.CMD_ABOUT:
                showAbout();
                break;
            case TerminalConst.CMD_VERSION:
                showVersion();
                break;
            case TerminalConst.CMD_INFO:
                showInfo();
                break;
            case TerminalConst.CMD_EXIT:
                exit();
                break;
        }
    }

    public static boolean parseArgs(final String[] args) {
        if (args == null || args.length == 0) return false;
        final String arg = args[0];
        parseArg(arg);
        return true;
    }

    private static void showInfo() {
        System.out.println("[INFO]");

        final  int availableProcessors = Runtime.getRuntime().availableProcessors();
        System.out.println("Available processors (cores): " + availableProcessors);

        final long freeMemory = Runtime.getRuntime().freeMemory();
        final String freeMemoryFormat = NumberUtil.formatBytes(freeMemory);
        System.out.println("Free memory: "+ freeMemoryFormat);

        final long maxMemory = Runtime.getRuntime().maxMemory();
        final String maxMemoryValue = NumberUtil.formatBytes(maxMemory);
        final String maxMemoryFormat = maxMemory == Long.MAX_VALUE ? "no limit" : maxMemoryValue;
        System.out.println("Maximum memory: " + maxMemoryFormat);

        final long totalMemory = Runtime.getRuntime().totalMemory();
        final String totalMemoryFormat = NumberUtil.formatBytes(totalMemory);
        System.out.println("Total memory available to JVM: " + totalMemoryFormat);

        final long usedMemory = totalMemory - freeMemory;
        final String usedMemoryFormat = NumberUtil.formatBytes(usedMemory);
        System.out.println("Used memory by JVM: " + usedMemoryFormat );
    }

    public static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("NAME: Pavel Bokhan");
        System.out.println("E-MAIL: shyaksh@gmail.com");
    }

    public static void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.0.0");
    }

    public static void showHelp() {
        System.out.println("[HELP]");
        System.out.println(TerminalCommand.ABOUT);
        System.out.println(TerminalCommand.VERSION);
        System.out.println(TerminalCommand.HELP);
        System.out.println(TerminalCommand.INFO);
        System.out.println(TerminalCommand.EXIT);
    }

    public static void exit() {
        System.exit(0);
    }

}
